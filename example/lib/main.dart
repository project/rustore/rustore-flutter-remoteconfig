import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rustore_remoteconfig/flutter_rustore_remoteconfig.dart';
import 'package:flutter_rustore_remoteconfig/rustore_remote_config.dart';

void main() {
  runApp(const MaterialApp(home: StartScreen()));
}

class Parameters extends StaticParameters {
  // @override
  // String? get deviceId => "42";
}

class StartScreen extends StatefulWidget {
  const StartScreen({super.key});

  @override
  State<StartScreen> createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: [
              const Text("Choose update behavior"),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => const ConfigScreen(
                          behavior: PluginUpdateBehavior.actualBehavior),
                    ),
                  );
                },
                child: const Text('Actual'),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => const ConfigScreen(
                          behavior: PluginUpdateBehavior.defaultBehavior),
                    ),
                  );
                },
                child: const Text('Default'),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => const ConfigScreen(
                        behavior: PluginUpdateBehavior.snapshotBehavior,
                      ),
                    ),
                  );
                },
                child: const Text('Snapshot'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ConfigScreen extends StatefulWidget {
  const ConfigScreen({super.key, required this.behavior});

  final PluginUpdateBehavior behavior;

  @override
  State<ConfigScreen> createState() => _ConfigScreenState();
}

class _ConfigScreenState extends State<ConfigScreen> {
  List<String> stack = [];
  String config = "";
  String appId = "ff73c68d-17e3-4adf-abdf-b4cd8c0563e6";

  @override
  void initState() {
    super.initState();
    remoteConfig();
  }

  void remoteConfig() {
    FlutterRustoreRemoteconfig.create(appId, widget.behavior, 15, Parameters(),
        onBackgroundJobErrors: (value) {
      final item = "onError: $value";
      setState(() {
        stack.add(item);
      });
    }, onFirstLoadComplete: () {
      debugPrint("First load complete");
      setState(() {
        stack.add("first load");
      });
    }, onMemoryCacheUpdated: () {
      debugPrint("memory cache updated");
      setState(() {
        stack.add("memory cache updated");
      });
    }, onInitComplete: () {
      debugPrint("init complete");
      setState(() {
        stack.add("init complete");
      });
    }, onPersistentStorageUpdated: () {
      debugPrint("onPersistentStorageUpdated");
      setState(() {
        stack.add("onPersistentStorageUpdated");
      });
    }, onRemoteConfigNetworkRequestFailure: (value) {
      final item = "request failure: $value";
      setState(() {
        stack.add(item);
      });
    });
  }

  void getRemoteConfig() {
    FlutterRustoreRemoteconfig.init();
    FlutterRustoreRemoteconfig.getRemoteConfig().then(((value) {
      setState(() {
        config = value;
      });
    }), onError: (err) {
      debugPrint("err: $err");
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Receive config screen'),
        ),
        body: Center(
          child: Column(
            children: [
              Text("Update Behavior: ${widget.behavior}"),
              TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Введите account',
                ),
                onChanged: (text) {
                  FlutterRustoreRemoteconfig.setAccount(text);
                  debugPrint("Entered value: $text");
                },
              ),
              TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Введите language',
                ),
                onChanged: (text) {
                  FlutterRustoreRemoteconfig.setLanguage(text);
                  debugPrint("Entered value lang: $text");
                },
              ),
              OutlinedButton(
                onPressed: () {
                  FlutterRustoreRemoteconfig.init();
                },
                child: const Text('init()'),
              ),
              OutlinedButton(
                onPressed: () {
                  getRemoteConfig();
                },
                child: const Text('getRemoteConfig()'),
              ),
              Text("Received Config: $config"),
              const SizedBox(height: 8),
              for (final item in stack) ...[
                Text(item),
                const SizedBox(height: 4),
              ],
            ],
          ),
        ),
      ),
    );
  }
}
