# flutter_rustore_remoteconfig

Flutter RuStore SDK для работы с  RuStore remote config.

## Общее

### Пример реализации

Для того, чтобы узнать как правильно интегрировать платежи, рекомендуется ознакомиться с [приложением-примером](https://gitflic.ru/project/rustore/rustore-flutter-remoteconfig)

### Подключение в проект

Для подключения пакета к проекту нужно выполнить команду

```shell
flutter pub add flutter_rustore_remoteconfig
```

Эта команда добавит строчку в файл pubspec.yaml

```groovy
dependencies:
    flutter_rustore_remoteconfig: ^7.0.0
```

### Создание клиента

Для работы с конфигом, нам надо создать RemoteConfigClient с помощью метода

```dart
create(
    String appId,
    PluginUpdateBehavior updateBehavior,
    int interval, StaticParameters parameters,
    {Function? onBackgroundJobErrors,
    Function? onFirstLoadComplete,
    Function? onMemoryCacheUpdated,
    Function? onInitComplete,
    Function? onPersistentStorageUpdated,
    Function? onRemoteConfigNetworkRequestFailure}
    )`.
```

```dart
FlutterRustoreRemoteconfig.create(
    appId,
    behavior,
    interval,
    Parameters(),
    onBackgroundJobErrors: (value) {
      
    }, 
    onFirstLoadComplete: (value) {
      
    },
    onMemoryCacheUpdated: (value) {
      
    }, 
    onInitComplete: (value) {
      
    }, 
    onPersistentStorageUpdated: (value) {
      
    }, 
    onRemoteConfigNetworkRequestFailure: (value) {
      
    });
```

`appId` - Application id вашего проекта [Remote config](https://remote-config.rustore.ru);
`behavior` - Политика обновления SDK ;
`interval` - Время для изменения политики обновления snapshot;
`parameters` - Статические параметры SDK;

```dart
Function? onBackgroundJobErrors,
Function? onFirstLoadComplete,
Function? onMemoryCacheUpdated,
Function? onInitComplete,
Function? onPersistentStorageUpdated,
Function? onRemoteConfigNetworkRequestFailure``` - Слушатели событий СДК;

### Получение конфигурации Remote Config

```dart
FlutterRustoreRemoteconfig.getRemoteConfig().then(((value) {
      
    }), onError: (err) {
      debugPrint("err: $err");
    });
```

Метод `getRemoteConfig` возвращает нам экземпляр RemoteConfig — это текущий набор всех данных, полученных в зависимости от выбранной политики обновления при инициализации, экземпляр имеет весь набор ключей, которые были переданы с сервера в зависимости от параметров, указанных при инициализации.

Класс RemoteConfig имеет методы получения данных и приведения его к определенным типам.

`containsKey(String key);`
`getString(String key);`
`getBool(String key);`
`getNum(String key);`

`key` - Параметр, созданный в консоли Remote config.
