package ru.rustore.flutter_rustore_remoteconfig.flutter_rustore_remoteconfig

import ru.rustore.sdk.remoteconfig.Account
import ru.rustore.sdk.remoteconfig.ConfigRequestParameter
import ru.rustore.sdk.remoteconfig.ConfigRequestParameterProvider
import ru.rustore.sdk.remoteconfig.Language

object ConfigRequestParameterProviderImpl : ConfigRequestParameterProvider {
    private var language: Language? = null
    private var account: Account? = null

    fun setAccount(value: String) {
        account = Account(value)
    }

    fun setLanguage(value: String) {
        language = Language(value)
    }

    fun getAccount(): String {
        return account?.value.orEmpty()
    }

    fun getLanguage(): String {
        return language?.value.orEmpty()
    }

    override fun getConfigRequestParameter(): ConfigRequestParameter {
        return ConfigRequestParameter(
            language = language,
            account = account
        )
    }
}