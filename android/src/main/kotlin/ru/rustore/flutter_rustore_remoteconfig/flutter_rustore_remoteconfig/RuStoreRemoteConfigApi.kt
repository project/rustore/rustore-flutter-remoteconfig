package ru.rustore.flutter_rustore_remoteconfig.flutter_rustore_remoteconfig

import android.app.Application
import com.google.gson.GsonBuilder
import ru.rustore.sdk.remoteconfig.AppBuild
import ru.rustore.sdk.remoteconfig.AppId
import ru.rustore.sdk.remoteconfig.AppVersion
import ru.rustore.sdk.remoteconfig.DeviceId
import ru.rustore.sdk.remoteconfig.DeviceModel
import ru.rustore.sdk.remoteconfig.Environment
import ru.rustore.sdk.remoteconfig.OsVersion
import ru.rustore.sdk.remoteconfig.RemoteConfig
import ru.rustore.sdk.remoteconfig.RemoteConfigClient
import ru.rustore.sdk.remoteconfig.RemoteConfigClientBuilder
import ru.rustore.sdk.remoteconfig.UpdateBehaviour
import kotlin.time.Duration.Companion.seconds
import kotlin.time.DurationUnit
import kotlin.time.toDuration

class RuStoreRemoteConfigApi(private val application: Application) : RuStoreRemoteConfig {

    private lateinit var remoteConfig: RemoteConfig

    private var isInitialized = false

    override fun create(
        appId: String,
        updateBehavior: PluginUpdateBehavior,
        interval: Long,
        parameters: StaticParameters,
        callback: (Result<Unit>) -> Unit
    ) {

        if (isInitialized) return

        val behavior = when (updateBehavior) {
            PluginUpdateBehavior.ACTUAL_BEHAVIOR -> UpdateBehaviour.Actual
            PluginUpdateBehavior.SNAPSHOT_BEHAVIOR -> UpdateBehaviour.Snapshot(
                interval.toDuration(
                    DurationUnit.MINUTES
                )
            )

            PluginUpdateBehavior.DEFAULT_BEHAVIOR -> UpdateBehaviour.Default(
                5.seconds
            )
        }

        val environment = when (parameters.environment) {
            EnvironmentEnum.ALPHA -> Environment.ALPHA
            EnvironmentEnum.BETA -> Environment.BETA
            EnvironmentEnum.RELEASE -> Environment.RELEASE
            null -> null
        }

        RemoteConfigClientBuilder(appId = AppId(appId), context = application)
            .setConfigRequestParameterProvider(ConfigRequestParameterProviderImpl)
            .setRemoteConfigClientEventListener(FlutterRemoteConfigEventListener())
            .setOsVersion(parameters.osVersion?.let { OsVersion(it) })
            .setDevice(parameters.deviceModel?.let { DeviceModel(it) })
            .setDeviceId(parameters.deviceId?.let { DeviceId(it) })
            .setEnvironment(environment)
            .setAppVersion(parameters.appVersion?.let { AppVersion(it) })
            .setAppBuild(parameters.appBuild?.let { AppBuild(it) })
            .setUpdateBehaviour(behavior)
            .build()

        isInitialized = true
        Result.success("")
    }

    override fun getRemoteConfig(callback: (Result<String>) -> Unit) {
        val gson = GsonBuilder()
            .create()
        RemoteConfigClient.instance.getRemoteConfig().addOnSuccessListener {
            remoteConfig = it
            callback(Result.success(gson.toJson(it)))
        }.addOnFailureListener { throwable ->
            callback(Result.failure(throwable))
        }
    }

    override fun getString(key: String, callback: (Result<String>) -> Unit) {
        callback(Result.success(remoteConfig.getString(key)))
    }
    override fun getBoolean(key: String, callback: (Result<Boolean>) -> Unit) {
        callback(Result.success(remoteConfig.getBoolean(key)))
    }
    override fun getDouble(key: String, callback: (Result<Double>) -> Unit) {
        callback(Result.success(remoteConfig.getDouble(key)))
    }

    override fun containsKey(key: String, callback: (Result<Boolean>) -> Unit) {
        callback(Result.success(remoteConfig.containsKey(key)))
    }

    override fun setAccount(account: String, callback: (Result<Unit>) -> Unit) {
        ConfigRequestParameterProviderImpl.setAccount(account)
    }

    override fun setLanguage(language: String, callback: (Result<Unit>) -> Unit) {
        ConfigRequestParameterProviderImpl.setLanguage(language)
    }

    override fun getAccount(callback: (Result<String>) -> Unit) {
        ConfigRequestParameterProviderImpl.getAccount()
        callback(Result.success(ConfigRequestParameterProviderImpl.getAccount()))
    }

    override fun getLanguage(callback: (Result<String>) -> Unit) {
        ConfigRequestParameterProviderImpl.getLanguage()
        callback(Result.success(ConfigRequestParameterProviderImpl.getLanguage()))
    }

    override fun init(callback: (Result<Unit>) -> Unit) {
        RemoteConfigClient.instance.init()
    }
}