package ru.rustore.flutter_rustore_remoteconfig.flutter_rustore_remoteconfig

import android.os.Handler
import android.os.Looper
import ru.rustore.sdk.remoteconfig.RemoteConfigClientEventListener
import ru.rustore.sdk.remoteconfig.RemoteConfigException

class FlutterRemoteConfigEventListener: RemoteConfigClientEventListener {

    companion object {
        var client: RuStoreRemoteConfigCallback? = null
    }

    private val uiThreadHandler: Handler = Handler(Looper.getMainLooper())

    override fun backgroundJobErrors(exception: RemoteConfigException.BackgroundConfigUpdateError) {
        uiThreadHandler.post {
          client?.backgroundJobErrors(exception.message) {}
        }
    }

    override fun firstLoadComplete() {
        uiThreadHandler.post {
            client?.firstLoadComplete {}
        }
    }

    override fun initComplete() {
        uiThreadHandler.post {
            client?.onInitCompleted {}
        }
    }

    override fun memoryCacheUpdated() {
        uiThreadHandler.post {
            client?.memoryCacheUpdated {}
        }
    }

    override fun persistentStorageUpdated() {
        uiThreadHandler.post {
            client?.persistentStorageUpdated {}
        }
    }

    override fun remoteConfigNetworkRequestFailure(throwable: Throwable) {
        uiThreadHandler.post {
            throwable.message?.let { client?.remoteConfigNetworkRequestFailure(it) {} }
        }
    }
}