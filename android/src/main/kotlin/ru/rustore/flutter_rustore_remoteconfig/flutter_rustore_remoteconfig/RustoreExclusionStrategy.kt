package ru.rustore.flutter_rustore_remoteconfig.flutter_rustore_remoteconfig

import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes
import ru.rustore.sdk.remoteconfig.RemoteConfigException

class RuStoreExclusionStrategy: ExclusionStrategy {
    override fun shouldSkipField(field: FieldAttributes): Boolean {
        return field.declaringClass == RemoteConfigException::class.java ||
                field.declaringClass == Throwable::class.java
    }

    override fun shouldSkipClass(clazz: Class<*>?): Boolean {
        return false
    }

}