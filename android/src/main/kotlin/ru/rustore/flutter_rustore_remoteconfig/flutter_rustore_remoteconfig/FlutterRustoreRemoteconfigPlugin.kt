package ru.rustore.flutter_rustore_remoteconfig.flutter_rustore_remoteconfig

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.annotation.NonNull
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding

/** FlutterRustoreRemoteconfigPlugin */
class FlutterRustoreRemoteconfigPlugin: FlutterPlugin, ActivityAware {
  private lateinit var context: Context
  private lateinit var application: Application

  override fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    context = flutterPluginBinding.applicationContext

    Log.d(
      "RuStoreRemoteConfigPlugin",
      "Trying to resolve Application from Context: ${context.javaClass.name}"
    )

    application = context as Application
    val client = RuStoreRemoteConfigApi(application)
    val callbacks = RuStoreRemoteConfigCallback(flutterPluginBinding.binaryMessenger)

    FlutterRemoteConfigEventListener.client = callbacks

    RuStoreRemoteConfig.setUp(flutterPluginBinding.binaryMessenger, client)
  }

  override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {}

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {}

  override fun onDetachedFromActivityForConfigChanges() {}

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {}

  override fun onDetachedFromActivity() {}
}
