# 8.0.0

Updated Rustore Remote Config SDK to version 8.0.0.
Add support gradle 8

# 7.0.0

Updated Rustore Remote Config SDK to version 7.0.0.

# 6.1.0

Updated Rustore Remote Config SDK to version 6.1.0.

# 6.0.1

Internal fixes

# 6.0.0

Updated Rustore Remote Config SDK to version 6.

# 1.0.0

Created plugin for Rustore Remote Config SDK.
