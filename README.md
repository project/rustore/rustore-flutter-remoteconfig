# flutter_rustore_remoteconfig

Flutter RuStore SDK для работы с  RuStore remote config.

- [flutter\_rustore\_remoteconfig](#flutter_rustore_remoteconfig)
    - [Подготовка требуемых параметров](#подготовка-требуемых-параметров)
    - [Настройка примера приложения](#настройка-примера-приложения)
    - [Пример реализации](#пример-реализации)
    - [Подключение в проект](#подключение-в-проект)
    - [Создание клиента](#создание-клиента)
    - [Получение конфигурации Remote Config](#получение-конфигурации-remote-config)

### Подготовка требуемых параметров

Для запуска примера, вам нужны следующие параметры:

1. `app_id` - ваш AppId из консоли Remote Config(<https://remote-config.rustore.ru>).
2. Создайте в консоли параметры конфига.


### Настройка примера приложения

1. Замените значение переменной `app_id` в файле flutter_rustore_remoteconfig/example/lib/main.dart на ваш `app_id` из консоли.
2. Замените значение переменной `String? config = ""` на ваш параметр из консоли.

### Пример реализации

Для того, чтобы узнать как правильно интегрировать remote-config, рекомендуется ознакомиться с [приложением-примером](https://gitflic.ru/project/rustore/rustore-flutter-remoteconfig)

### Подключение в проект

Для подключения пакета к проекту нужно выполнить команду

```shell
flutter pub add flutter_rustore_remoteconfig
```

Эта команда добавит строчку в файл pubspec.yaml

```groovy
dependencies:
    flutter_rustore_remoteconfig: ^8.0.0
```

### Создание клиента

Для работы с конфигом, нам надо создать RemoteConfigClient с помощью метода

```dart
FlutterRustoreRemoteconfig.create(
    String appId,
    PluginUpdateBehavior updateBehavior,
    int interval, StaticParameters parameters,
    {Function? onBackgroundJobErrors,
    Function? onFirstLoadComplete,
    Function? onMemoryCacheUpdated,
    Function? onInitComplete,
    Function? onPersistentStorageUpdated,
    Function? onRemoteConfigNetworkRequestFailure}
    )`.
```

```dart
FlutterRustoreRemoteconfig.create(
    appId,
    behavior,
    interval,
    Parameters(),
    onBackgroundJobErrors: (value) {
      
    }, 
    onFirstLoadComplete: () {
      
    },
    onMemoryCacheUpdated: () {
      
    }, 
    onInitComplete: () {
      
    }, 
    onPersistentStorageUpdated: () {
      
    }, 
    onRemoteConfigNetworkRequestFailure: (value) {
      
    });
```

`appId` - Application id вашего проекта [Remote config](https://remote-config.rustore.ru);
`behavior` - Политика обновления SDK ;
`interval` - Время для изменения политики обновления snapshot;
`parameters` - Статические параметры SDK;

```dart
Function? onBackgroundJobErrors,
Function? onFirstLoadComplete,
Function? onMemoryCacheUpdated,
Function? onInitComplete,
Function? onPersistentStorageUpdated,
Function? onRemoteConfigNetworkRequestFailure``` - Слушатели событий СДК;
```

### Получение конфигурации Remote Config

```dart
FlutterRustoreRemoteconfig.getRemoteConfig().then(((value) {
      
    }), onError: (err) {
      debugPrint("err: $err");
    });
```

Метод `getRemoteConfig` возвращает нам экземпляр RemoteConfig — это текущий набор всех данных, полученных в зависимости от выбранной политики обновления при инициализации, экземпляр имеет весь набор ключей, которые были переданы с сервера в зависимости от параметров, указанных при инициализации.

Класс FlutterRustoreRemoteconfig имеет методы получения данных и приведения его к определенным типам.

`containsKey(String key);`
`getString(String key);`
`getBool(String key);`
`getInt(String key);`
`getDoube(String key);`

`key` - Параметр, созданный в консоли Remote config.
