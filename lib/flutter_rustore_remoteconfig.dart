import './rustore_remote_config.dart';

class RuStoreRemoteConfigCallbacks extends RuStoreRemoteConfigCallback {
  RuStoreRemoteConfigCallbacks(
      {this.onBackgroundJobErrors,
      this.onFirstLoadComplete,
      this.onMemoryCacheUpdated,
      this.onInitComplete,
      this.onPersistentStorageUpdated,
      this.onRemoteConfigNetworkRequestFailure});

  Function? onBackgroundJobErrors;
  Function? onFirstLoadComplete;
  Function? onMemoryCacheUpdated;
  Function? onInitComplete;
  Function? onPersistentStorageUpdated;
  Function? onRemoteConfigNetworkRequestFailure;

  @override
  Future<void> backgroundJobErrors(String error) async {
    if (onBackgroundJobErrors != null) {
      onBackgroundJobErrors!(error);
    }
  }

  @override
  Future<void> firstLoadComplete() async {
    if (onFirstLoadComplete != null) {
      onFirstLoadComplete!();
    }
  }

  @override
  Future<void> memoryCacheUpdated() async {
    if (onMemoryCacheUpdated != null) {
      onMemoryCacheUpdated!();
    }
  }

  @override
  Future<void> onInitCompleted() async {
    if (onInitComplete != null) {
      onInitComplete!();
    }
  }

  @override
  Future<void> persistentStorageUpdated() async {
    if (onPersistentStorageUpdated != null) {
      onPersistentStorageUpdated!();
    }
  }

  @override
  Future<void> remoteConfigNetworkRequestFailure(String error) async {
    if (onRemoteConfigNetworkRequestFailure != null) {
      onRemoteConfigNetworkRequestFailure!(error);
    }
  }
}

class FlutterRustoreRemoteconfig {
  static final RuStoreRemoteConfig _api = RuStoreRemoteConfig();

  static Future<void> create(String appId, PluginUpdateBehavior updateBehavior,
      int interval, StaticParameters parameters,
      {Function? onBackgroundJobErrors,
      Function? onFirstLoadComplete,
      Function? onMemoryCacheUpdated,
      Function? onInitComplete,
      Function? onPersistentStorageUpdated,
      Function? onRemoteConfigNetworkRequestFailure}) async {
    RuStoreRemoteConfigCallback.setUp(RuStoreRemoteConfigCallbacks(
        onBackgroundJobErrors: onBackgroundJobErrors,
        onFirstLoadComplete: onFirstLoadComplete,
        onMemoryCacheUpdated: onMemoryCacheUpdated,
        onInitComplete: onInitComplete,
        onPersistentStorageUpdated: onPersistentStorageUpdated,
        onRemoteConfigNetworkRequestFailure:
            onRemoteConfigNetworkRequestFailure));

    _api.create(appId, updateBehavior, interval, parameters);
    return;
  }

  static Future<void> setAccount(String account) async {
    return await _api.setAccount(account);
  }

  static Future<void> setLanguage(String language) async {
    return await _api.setLanguage(language);
  }

  static Future<void> init() {
    return _api.init();
  }

  static Future<String> getAccount() async {
    return await _api.getAccount();
  }

  static Future<String> getRemoteConfig() async {
    return await _api.getRemoteConfig();
  }

  static Future<String> getLanguage() async {
    return await _api.getLanguage();
  }

  static Future<String> getString(String key) async {
    return await _api.getString(key);
  }

  static Future<bool> getBoolean(String key) async {
    return await _api.getBoolean(key);
  }

  static Future<double> getDouble(String key) async {
    return await _api.getDouble(key);
  }

  static Future<bool> containsKey(String key) async {
    return await _api.containsKey(key);
  }

  static Future<int> getInt(String key) async {
    double result = await _api.getDouble(key);
    return result.toInt();
  }
}
