import 'package:pigeon/pigeon.dart';

@ConfigurePigeon(PigeonOptions(
    dartOut: 'lib/rustore_remote_config.dart',
    dartOptions: DartOptions(),
    kotlinOut: 'android/src/main/kotlin/pigeons/RuStoreRemoteConfig.kt',
    kotlinOptions: KotlinOptions(
        package:
            'ru.rustore.flutter_rustore_remoteconfig.flutter_rustore_remoteconfig'),
    dartPackageName: 'flutter_rustore_remoteconfig'))
enum EnvironmentEnum { alpha, beta, release }

class FlutterEnvironment {
  final EnvironmentEnum environment;

  FlutterEnvironment(this.environment);
}

enum PluginUpdateBehavior { actualBehavior, defaultBehavior, snapshotBehavior }

class FlutterUpdateBehavior {
  final PluginUpdateBehavior behavior;

  FlutterUpdateBehavior(this.behavior);
}

class StaticParameters {
  late String? osVersion;
  late String? deviceModel;
  late String? deviceId;
  late String? appVersion;
  late EnvironmentEnum? environment;
  late String? appBuild;
}

class DynamicParameters {
  late String? account;
  late String? language;

  DynamicParameters(this.account, this.language);
}

class RemoteConfigClientListenerCatcher {
  late Exception? exception;
  late String? throwable;

  RemoteConfigClientListenerCatcher(this.exception, this.throwable);
}

@HostApi()
abstract class RuStoreRemoteConfig {
  @async
  void create(String appId, PluginUpdateBehavior updateBehavior, int interval,
      StaticParameters parameters);

  @async
  String getRemoteConfig();

  @async
  void setAccount(String account);

  @async
  void setLanguage(String language);

  @async
  String getAccount();

  @async
  String getLanguage();

  @async
  String getString(String key);

  @async
  bool getBoolean(String key);

  @async
  double getDouble(String key);

  @async
  bool containsKey(String key);

  @async
  void init();
}

@FlutterApi()
abstract class RuStoreRemoteConfigCallback {
  @async
  void backgroundJobErrors(String error);

  @async
  void firstLoadComplete();

  @async
  void onInitCompleted();

  @async
  void memoryCacheUpdated();

  @async
  void persistentStorageUpdated();

  @async
  void remoteConfigNetworkRequestFailure(String error);
}
